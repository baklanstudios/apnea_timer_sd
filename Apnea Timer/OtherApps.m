//
//  OtherApps.m
//  Apnea Timer
//
//  Created by Mikhail on 08.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "OtherApps.h"
#import "OtherAppCell.h"

#import "Flurry.h"

/*
@interface NSArray (Map)

- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block;

@end

@implementation NSArray (Map)

- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [result addObject:block(obj, idx)];
    }];
    return result;
}

@end
*/

@interface OtherApps()
{
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *apps;
@end

@implementation OtherApps

-(void) viewDidLoad {
    [super viewDidLoad];
    [self initAppTable];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Initialization

-(void) initAppTable {
    _apps = @[
                  // ApneaTimerHD
                  @{@"image": @"app_ApneaTimerHD",
                    @"title": @"Apnea Timer HD",
                    @"appid": @"958951681",
                    @"descr": @"Apnea Timer for iPad"}
             ];
    /*
    NSArray *titles = [_apps mapObjectsUsingBlock:^(id obj, NSUInteger idx) {
        return obj[@"title"];
    }];
    
    NSArray *images = [_apps mapObjectsUsingBlock:^(id obj, NSUInteger idx) {
        return [NSString stringWithFormat:@"%@ / %@", obj[@"image"], obj[@"appid"]];
    }];
    */
}


#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = _apps[(int)indexPath.row];
    NSString *link = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@", dict[@"appid"]];

    [Flurry logEvent:@"see_other_app" withParameters:@{@"title" : dict[@"title"], @"id" : dict[@"appid"]}];
    
    [[UIApplication sharedApplication] openURL:
                    [NSURL URLWithString:link]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _apps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OtherAppCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDictionary *dict = _apps[(int)indexPath.row];
    
    [[cell.image layer] setCornerRadius:7];
    [cell.image setClipsToBounds:YES];
    cell.image.image = [UIImage imageNamed:dict[@"image"]];
    cell.title.text  = dict[@"title"];
    cell.descr.text  = dict[@"descr"];

    return cell;
}

@end
