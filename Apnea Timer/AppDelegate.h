//
//  AppDelegate.h
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@class CSPausibleTimer;

@class CO2TableClass;

@interface AppDelegate : UIResponder <
                                        UIApplicationDelegate,
                                        UITabBarControllerDelegate,
                                        ADBannerViewDelegate
                                     >
@property (strong, nonatomic) UIWindow           *window;
@property (strong, nonatomic) CSPausibleTimer    *timer;
@property (strong, nonatomic) ADBannerView       *adBanner;
@property (weak, nonatomic)   UITabBarController *tabBarVC;
@property BOOL bannerIsVisible;
@property BOOL purchased;
@property BOOL useSound;
@property BOOL useAdditionalSound;

@property int  timeBaseO2;
@property int  timeBaseCO2;

@property int  timeBaseTriA;
@property int  repeatsTriA;

@property int  timeBaseTriB;
@property int  repeatsTriB;

@property int  timeBaseSqA;
@property int  repeatsSqA;

@property int  timeBaseSqB;
@property int  repeatsSqB;

@property int  timeBaseSqC;
@property int  repeatsSqC ;

@property BOOL needsSave;

- (void) hideAdBanner;
- (void) disableTabBar;
- (void) enableTabBar;
- (void) saveSettings;

@end
