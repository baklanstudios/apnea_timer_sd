//
//  ViewControllerPrototype.h
//  Apnea Timer
//
//  Created by Mikhail on 02.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShrinkViewProtocol.h"
#import "AppDelegate.h"

@interface ViewControllerPrototype : UIViewController <ShrinkViewProtocol>

@property (weak, nonatomic) AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstrContainerHeight;

@end
