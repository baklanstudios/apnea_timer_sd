//
//  LoggedViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 23.11.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewControllerPrototype.h"

@interface LoggedViewController : ViewControllerPrototype

@end
