//
//  LoggedViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 23.11.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "LoggedViewController.h"
#import "Flurry.h"

@interface LoggedViewController ()

@end

@implementation LoggedViewController

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Flurry logPageView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
