//
//  UITimerViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 11.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//


//#import <AudioToolbox/AudioToolbox.h>
#import <Accounts/Accounts.h>
#import "UIButton+BackgroundColor.h"

#import "TTTimerViewController.h"
#import "UITimerViewController.h"
#import "AppDelegate.h"
#import "CSPausibleTimer.h"

#import "TrainingTableEx.h"
#import "CO2TableClass.h"
#import "O2TableClass.h"

#import "Tools.h"

#import "Flurry.h"

#pragma mark - Macros

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface UITimerViewController()
{
    SystemSoundID soundClickID;
    SystemSoundID soundDingID;
    SystemSoundID soundDoneID;
    int lastPhase;
    
    UIColor *green,  *greenA;
    UIColor *blue,   *blueA;
    UIColor *red,    *redA;
    UIColor *orange, *orangeA;
    UIColor *grey;
    UIColor *lightGrey;
    
    BOOL canPlay;
    BOOL cleanTraining;
    
    float timerStep;
}

@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@property (weak, nonatomic) IBOutlet UILabel  *lblPhaseTitle;
@property (weak, nonatomic) IBOutlet UILabel  *lblPhaseTime;
@property (weak, nonatomic) IBOutlet UILabel  *lblNextPhase;
@property (weak, nonatomic) IBOutlet UILabel  *lblTimeLeft;

@property (weak, nonatomic) IBOutlet UILabel  *lblTimeLeftTitle;
@property (weak, nonatomic) IBOutlet UILabel  *lblNextPhaseTitle;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *shareButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnShowTrainingTable;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnTable;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnShare;

@property (strong, nonatomic) NSDate *startTime;

@end

@implementation UITimerViewController

static float timerStepShort      = 0.033f;
static float timerStepLong       = 0.25f;
//static int   shortTimerThreshold = 5;

#pragma mark - ViewController Functions

-(void) viewDidLoad {
    
    [super viewDidLoad];
    lastPhase = -1;
    [self initColors];
    [self setButtons];
    [self expandViewAnimated:NO];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];


    self.trainingTable.delegate = self;

    if (!self.appDelegate.timer.isValid) {
        [self.trainingTable reset];
        [self.trainingTable nextPhase:YES];
        lastPhase = 0;
        [self setTimerStep];
        [self setLabels];
    }
    
    if ([self isKindOfClass:[TTTimerViewController class]]) {
        [self.appDelegate disableTabBar];
    } else {
        [self.appDelegate enableTabBar];
        self.navigationItem.backBarButtonItem.enabled = NO;
    }

    canPlay = YES;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showTrainingTable"]) {
        [[segue destinationViewController] setTrainingTable:self.trainingTable];
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", nil)
                    style:UIBarButtonItemStyleBordered
                   target:nil
                   action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showTrainingTable"]) {
        return ![self.appDelegate.timer isActive];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Sound Functions

- (void) playSound:(NSString *)name withExtension:(NSString *)extension
{
    if (self.appDelegate.useSound != 0) {
        NSURL* soundUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:name ofType:extension]];
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
        self.player.delegate = self;
        self.player.volume = 1.0;
        [self.player play];
    }
}

- (void) playClickSound {
    [self playSound:@"click2" withExtension:@"wav"];
}

- (void) playPhaseSound {
    [self playSound:@"ding" withExtension:@"wav"];
}

- (void) playMiddlePhaseSound {
    [self playSound:@"boop2" withExtension:@"wav"];
}

- (void) play15secSound {
    [self playMiddlePhaseSound];
    [NSTimer scheduledTimerWithTimeInterval:0.20f
                                     target:self
                                   selector:@selector(playMiddlePhaseSound)
                                   userInfo:nil
                                    repeats:NO];
}

- (void) play321Sound {
    [self playSound:@"click2" withExtension:@"wav"];
}

- (void) playDoneSound {
    [self playPhaseSound];
    [NSTimer scheduledTimerWithTimeInterval:0.2f
                                     target:self
                                   selector:@selector(playPhaseSound)
                                   userInfo:nil
                                    repeats:NO];
//    [NSTimer scheduledTimerWithTimeInterval:0.4f
//                                     target:self
//                                   selector:@selector(playPhaseSound)
//                                   userInfo:nil
//                                    repeats:NO];
}

- (BOOL) shouldPlayAdditionalSounds {
    return ([self.trainingTable isKindOfClass:[CO2TableClass class]] ||
            [self.trainingTable isKindOfClass:[O2TableClass class]])
         && self.appDelegate.useAdditionalSound && self.appDelegate.useSound;
}


#pragma mark - TimerViewProtocol Delegate Methods

-(void) setLabels {
    self.lblNextPhaseTitle.text = NSLocalizedString(@"next", nil);
    self.lblTimeLeftTitle.text = NSLocalizedString(@"time left", nil);
    [self updateTotalTime];
    [self updatePhaseTime];
    [self setNextPhase];
    [self setActivePhase];
}

- (void) updateTotalTime {
    self.lblTimeLeft.text = [NSString stringWithFormat:@"%@", [Tools secondsToTimeStr:[self.trainingTable getTotalTime]]];
}

- (void) updatePhaseTime {
    
    BOOL tens = (timerStep == timerStepLong) ? NO : YES;
    
    self.lblPhaseTime.text = [NSString stringWithFormat:@"%@", [Tools secondsToTimeStr:[self.trainingTable phaseDuration] withTens:tens]];
    
    float max = [self.trainingTable getInitialPhaseDuration:self.trainingTable.currentPhase forStep:self.trainingTable.currentStep];
    
    float val = (max - [self.trainingTable phaseDuration]) / max;
    
    [self.progressView setProgress:val animated:NO];
    
}

- (void) setActivePhase {
//    [self setTimerStep];
//    if ([self.appDelegate.timer isActive])
//        [self timerRestart];
    self.lblPhaseTitle.text = [self.trainingTable phaseDescription];
    [self.progressView setProgress:0.0f animated:NO];
}

- (void) setNextPhase {
    float     nextPhaseDur = [self.trainingTable getNextPhaseDuration];
    NSString *nextPhaseDsc = [self.trainingTable getNextPhaseDescription];
    
    if (nextPhaseDur >= 0 && nextPhaseDsc != nil) {
        self.lblNextPhase.text = [NSString stringWithFormat:@"%@ (%@)", nextPhaseDsc, [Tools secondsToTimeStr:nextPhaseDur]];
    } else {
        self.lblNextPhase.text = NSLocalizedString(@"done", nil);
    }
}


#pragma mark - Timer Functions

-(void) timerStart {
    if (self.appDelegate.timer.isPaused) {
        [self.appDelegate.timer start];
    } else {
        self.appDelegate.timer = [CSPausibleTimer timerWithTimeInterval:timerStep
                                                                 target:self
                                                               selector:@selector(timeStep)
                                                               userInfo:nil
                                                                repeats:YES];
        [self.appDelegate.timer start];
    }
}

- (void) timerPause {
    [self.appDelegate.timer pause];
}

- (void) timerClear {
    [self.appDelegate.timer invalidate];
    self.appDelegate.timer = nil;
}

/*
- (void) timerRestart {
    [self timerClear];
    [self timerStart];
}
*/

- (void) setTimerStep {
    if ([self.trainingTable isKindOfClass:[CO2TableClass class]] || [self.trainingTable isKindOfClass:[O2TableClass class]]) {
        //timerStep = ([self.trainingTable phaseDuration] > shortTimerThreshold) ? timerStepLong : timerStepShort;
        timerStep = timerStepLong;
    } else {
        timerStep = timerStepShort;
    }
}

- (void) timeStep {
    if (self.trainingTable.currentStep < 0) {
        [self trainingDone];
    } else {
        [self.trainingTable timeStep:timerStep];
        if (lastPhase != self.trainingTable.currentPhase &&
            [self.appDelegate.timer isActive]) {
            if (canPlay)
                [self playPhaseSound];
            else
                canPlay = TRUE;
        }
        lastPhase = self.trainingTable.currentPhase;
    }
    /*
    if ([self.trainingTable phaseDuration] <= shortTimerThreshold && timerStep == timerStepLong) {
        timerStep = timerStepShort;
        [self timerRestart];
    }
    */
}

#pragma mark - User Action Functions

-(void) trainingDone {
    NSDictionary *d = [self makeFlurryParamsWithTime];
    if (d) [Flurry logEvent:@"training_done" withParameters:d];
    if (d && cleanTraining) [Flurry logEvent:@"training_complete" withParameters:d];
    [self playDoneSound];
    [self actionReset];
}

-(void) actionStart {
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
     cleanTraining = YES;
     self.startTime = [NSDate date];
    [self enablePauseButton];
    [self enableSkipButton];
    [self timerStart];

    self.btnTable.enabled = NO;
    self.btnShare.enabled = NO;
    [self.appDelegate disableTabBar];
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

-(void) actionPause {
    [self enableContinueButton];
    [self enableResetButton];
    [self timerPause];
}

-(void) actionSkip {
    cleanTraining = NO;
    canPlay = NO;
    [self timerClear];
    [self.trainingTable nextPhase:YES];
    if (self.trainingTable.currentPhase >= 0 && self.trainingTable.currentStep >= 0) {
        [self setLabels];
        [self timerStart];
    } else {
        [self actionReset];
    }
}

-(void) actionReset {
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
     self.btnTable.enabled = YES;
     self.btnShare.enabled = YES;
    [self enableStartButton];
    [self disableResetButton];
    [self timerClear];
    [self.trainingTable reset];
    [self.trainingTable nextPhase:YES];
    [self setLabels];
    if ([self isKindOfClass:[TTTimerViewController class]]) {
        [self.navigationItem setHidesBackButton:NO animated:YES];
    } else {
        [self.appDelegate enableTabBar];
        self.navigationItem.backBarButtonItem.enabled = NO;
    }
    lastPhase = 0;
}

-(void) actionFirstButtonPressed {
    [self playClickSound];
    NSDictionary *d = [self makeFlurryParams];
    if (self.appDelegate.timer.isValid) {
        if (d) [Flurry logEvent:@"training_pause" withParameters:d];
        [self actionPause];
    } else {
        if (d) [Flurry logEvent:@"training_start" withParameters:d];
        [self actionStart];
    }
}

-(void) actionSecondButtonPressed {
    [self playClickSound];
    NSDictionary *d = [self makeFlurryParams];
    if (self.appDelegate.timer.isValid) {
        if (d) [Flurry logEvent:@"training_skip" withParameters:d];
        [self actionSkip];
    } else {
        if (d) [Flurry logEvent:@"training_reset" withParameters:d];
        [self actionReset];
    }
}


#pragma mark - User Interface Functions

- (void) enableStartButton {
     self.btnStart.layer.borderColor = [green CGColor];
    [self.btnStart setTitleColor:green  forState:UIControlStateNormal];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self.btnStart setTitleColor:greenA forState:UIControlStateHighlighted];
     self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
}

- (void) enableContinueButton {
     self.btnStart.layer.borderColor = [green CGColor];
    [self.btnStart setTitleColor:green  forState:UIControlStateNormal];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self.btnStart setTitleColor:greenA forState:UIControlStateHighlighted];
     self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Continue", nil) forState:UIControlStateNormal];
}

- (void) enablePauseButton {
     self.btnStart.layer.borderColor = [orange CGColor];
    [self.btnStart setTitleColor:orange  forState:UIControlStateNormal];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self.btnStart setTitleColor:orangeA forState:UIControlStateHighlighted];
     self.btnStart.enabled = YES;
    [self.btnStart setTitle:NSLocalizedString(@"Pause", nil) forState:UIControlStateNormal];
}

- (void) enableSkipButton {
     self.btnSkip.layer.borderColor = [blue CGColor];
    [self.btnSkip setTitleColor:blue  forState:UIControlStateNormal];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self.btnSkip setTitleColor:blueA forState:UIControlStateHighlighted];
     self.btnSkip.enabled = YES;
    [self.btnSkip setTitle:NSLocalizedString(@"Skip", nil) forState:UIControlStateNormal];
}

- (void) enableResetButton {
     self.btnSkip.layer.borderColor = [red CGColor];
    [self.btnSkip setTitleColor:red  forState:UIControlStateNormal];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
        [self.btnSkip setTitleColor:redA forState:UIControlStateHighlighted];
     self.btnSkip.enabled = YES;
    [self.btnSkip setTitle:NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
}

- (void) disableResetButton {
     self.btnSkip.layer.borderColor = [lightGrey CGColor];
    [self.btnSkip setTitleColor:lightGrey forState:UIControlStateNormal];
     self.btnSkip.enabled = NO;
    [self.btnSkip setTitle:NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
}

- (void) setButtons {
    [[self.btnStart layer] setMasksToBounds:YES];
    [[self.btnStart layer] setCornerRadius:self.btnStart.bounds.size.width / 2];
    [[self.btnStart layer] setBorderWidth:2.0f];

    [[self.btnSkip layer] setMasksToBounds:YES];
    [[self.btnSkip layer] setCornerRadius:self.btnStart.bounds.size.width / 2];
    [[self.btnSkip layer] setBorderWidth:2.0f];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self.btnStart setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
        [self.btnSkip  setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
    }
    
    [self enableStartButton];
    [self disableResetButton];
}

-(void) initColors {
    green     = [UIColor colorWithRed:(97/255.0)    green:(160/255.0)   blue:(5/255.0)      alpha:  1];
    greenA    = [UIColor colorWithRed:(97/255.0)    green:(160/255.0)   blue:(5/255.0)      alpha:0.3];
    orange    = [UIColor colorWithRed:(255/255.0)   green:(164/255.0)   blue:(96/255.0)     alpha:  1];
    orangeA   = [UIColor colorWithRed:(255/255.0)   green:(164/255.0)   blue:(96/255.0)     alpha:0.3];
    red       = [UIColor colorWithRed:(255/255.0)   green:(99/255.0)    blue:(71/255.0)     alpha:  1];
    redA      = [UIColor colorWithRed:(255/255.0)   green:(99/255.0)    blue:(71/255.0)     alpha:0.3];
    blue      = [UIColor colorWithRed:(100/255.0)   green:(149/255.0)   blue:(237/255.0)    alpha:  1];
    blueA     = [UIColor colorWithRed:(100/255.0)   green:(149/255.0)   blue:(237/255.0)    alpha:0.3];
    grey      = [UIColor colorWithRed:(97/255.0)    green:(160/255.0)   blue:(5/255.0)      alpha:  1];
    lightGrey = [UIColor colorWithRed:(139/255.0)   green:(136/255.0)   blue:(120/255.0)    alpha:  1];
}

-(NSDictionary*) makeFlurryParamsWithTime {
    NSNumber *time = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSinceDate:self.startTime]];
    NSMutableDictionary *d = [[self makeFlurryParams] mutableCopy];
    [d setObject:time forKey:@"time"];
    return (NSDictionary*)d;
}

-(NSDictionary*) makeFlurryParams {
    if (!self.trainingTable) return nil;
    NSDictionary *params;
    if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
        TrainingTableEx *tt = (TrainingTableEx*)self.trainingTable;
        params = @{
                   @"table"   : [tt getName],
                   @"tbase"   : [NSNumber numberWithInt:tt.timeBase],
                   @"repeats" : [NSNumber numberWithInt:tt.repeats],
                   };
    } else {
        params = @{
                   @"table"   : [self.trainingTable getName],
                   @"tbase"   : [NSNumber numberWithInt:self.trainingTable.timeBase],
                   };
    }
    return params;
}

@end
