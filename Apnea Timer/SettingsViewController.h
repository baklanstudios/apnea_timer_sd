//
//  SettingsViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 21.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SocialViewController.h"

@class AppDelegate;

@interface SettingsViewController : SocialViewController <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (weak, nonatomic) IBOutlet UISwitch *soundSwitch;
//@property (weak, nonatomic) AppDelegate *appDelegate;

@end
