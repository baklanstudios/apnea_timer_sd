//
//  TTTimerView.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TTTimerViewController.h"
#import "AppDelegate.h"
#import "Tools.h"

@interface TTTimerViewController()

@end

@implementation TTTimerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)btnStartPressed:(id)sender {
    [self actionFirstButtonPressed];
}

- (IBAction)btnResetPressed:(id)sender {
    [self actionSecondButtonPressed];
}

@end
