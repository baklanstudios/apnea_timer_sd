//
//  TrainingTableCellEx4.h
//  Apnea Timer
//
//  Created by Mikhail on 13.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableCell4.h"

@interface TrainingTableCellEx4 : TrainingTableCell4

@property (weak, nonatomic) IBOutlet UILabel *lblCycles;

@end
