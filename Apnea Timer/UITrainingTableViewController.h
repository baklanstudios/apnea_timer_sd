//
//  UITrainingTableViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 11.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShrinkViewProtocol.h"

#import "LoggedViewController.h"

@class AppDelegate;
@class TrainingTable;

@interface UITrainingTableViewController : ViewControllerPrototype <UITableViewDelegate, UITableViewDataSource>

//@property (weak, nonatomic) AppDelegate *appDelegate;
@property (weak, nonatomic) TrainingTable *trainingTable;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
