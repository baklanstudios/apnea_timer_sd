//
//  TrainingTablesViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TTListViewController.h"
#import "TTTimerViewController.h"
#import "AppDelegate.h"
#import "TrainingTableEx.h"
#import "TriTableClass.h"
#import "SqTableClass.h"

@interface TTListViewController ()

@property (strong, nonatomic) NSArray *tables;
@property (strong, nonatomic) NSArray *tableNames;

@end

@implementation TTListViewController

#pragma mark - View Controller

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    [self initTrainingTables];
//    self.screenName = @"Training Tables List Screen";
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.appDelegate enableTabBar];
    if (self.appDelegate.bannerIsVisible)
        [self shrinkViewAnimated:NO];
}

- (void) initTrainingTables {
    NSString *breathing = NSLocalizedString(@"breathing", nil);
    
    self.tableNames = @[
                        [NSString stringWithFormat:@"\u25B3 %@ (A)", breathing],
                        [NSString stringWithFormat:@"\u25B3 %@ (B)", breathing],
                        [NSString stringWithFormat:@"\u25A2 %@ (A)", breathing],
                        [NSString stringWithFormat:@"\u25A2 %@ (B)", breathing],
                        [NSString stringWithFormat:@"\u25A2 %@ (C)", breathing]
                        ];

    self.tables = @[
                    [[TriTableClass alloc] initTableA:@"TriA"//@"\u25B3 breathing (A)"
                                         withTimeBase:self.appDelegate.timeBaseTriA
                                            andRepeat:self.appDelegate.repeatsTriA],
                    
                    [[TriTableClass alloc] initTableB:@"TriB"//@"\u25B3 breathing (B)"
                                         withTimeBase:self.appDelegate.timeBaseTriB
                                            andRepeat:self.appDelegate.repeatsTriB],
                    
                    [[SqTableClass  alloc] initTableA:@"SqrA"//@"\u25A2 breathing (A)"
                                         withTimeBase:self.appDelegate.timeBaseSqA
                                            andRepeat:self.appDelegate.repeatsSqA],
                    
                    [[SqTableClass  alloc] initTableB:@"SqrB"//@"\u25A2 breathing (B)"
                                         withTimeBase:self.appDelegate.timeBaseSqB
                                            andRepeat:self.appDelegate.repeatsSqB],
                    
                    [[SqTableClass  alloc] initTableC:@"SqrC"//@"\u25A2 breathing (C)"
                                         withTimeBase:self.appDelegate.timeBaseSqC
                                            andRepeat:self.appDelegate.repeatsSqC]
                    ];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tables.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *object = self.tableNames[indexPath.row];
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showTimerPage"]) {

        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *title = self.tableNames[indexPath.row]; //self.trainingTables[indexPath.row];
        TTTimerViewController *vc = [segue destinationViewController];
        vc.navigationItem.title = title;
        [vc setTrainingTable:self.tables[indexPath.row]];
        
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"back", nil)
                                                                       style:UIBarButtonItemStyleBordered
                                                                      target:nil
                                                                      action:nil];
        [[self navigationItem] setBackBarButtonItem:backButton];
        
    }
}

@end
