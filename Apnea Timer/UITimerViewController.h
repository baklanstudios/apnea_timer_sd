//
//  UITimerViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 11.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "TrainingTable.h"
#import "ViewControllerPrototype.h"
#import "SocialViewController.h"

@class AppDelegate;

@interface UITimerViewController : SocialViewController <TimerViewProtocol, AVAudioPlayerDelegate>

@property (strong, nonatomic) TrainingTable *trainingTable;
@property (strong, nonatomic) AVAudioPlayer *player;

@end
