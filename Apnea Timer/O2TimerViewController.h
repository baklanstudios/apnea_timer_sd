//
//  O2TimerViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITimerViewController.h"

@interface O2TimerViewController : UITimerViewController

@end

@interface O2TimerViewController (Protected)

-(void) actionFirstButtonPressed;
-(void) actionSecondButtonPressed;

@end