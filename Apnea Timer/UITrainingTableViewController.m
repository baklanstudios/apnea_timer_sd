//
//  UITrainingTableViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 11.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "UITrainingTableViewController.h"
#import "AppDelegate.h"
#import "TrainingTable.h"
#import "TrainingTableEx.h"

#import "TrainingTableCellSimple.h"
#import "TrainingTableCell2.h"
#import "TrainingTableCellEx2.h"
#import "TrainingTableCell3.h"
#import "TrainingTableCellEx3.h"
#import "TrainingTableCell4.h"
#import "TrainingTableCellEx4.h"

#import "Tools.h"

@interface UITrainingTableViewController()
{
    CGRect initialRect;
}
@property (weak, nonatomic) IBOutlet UISlider *sldrTimeBase;
@property (weak, nonatomic) IBOutlet UISlider *sldrRepeats;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeBase;
@property (weak, nonatomic) IBOutlet UILabel *lblRepeats;

@end

@implementation UITrainingTableViewController

#pragma mark - View Controller
- (void) viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    initialRect = self.tableView.frame;
    self.tableView.allowsSelection = NO;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.appDelegate disableTabBar];

    self.sldrTimeBase.value = self.trainingTable.timeBase;
    [self setTimeBaseLabel];
    
    if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
        TrainingTableEx *tt = (TrainingTableEx*)self.trainingTable;
        self.sldrRepeats.value = tt.repeats;
        [self setRepeatsLabel];
    }
//    [self logTimeBase];
}

-(void) logTimeBase {
    if ([[self.trainingTable.tableName uppercaseString]        isEqualToString:@"CO2"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseCO2);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"O2"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseO2);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"TRIA"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseTriA);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"TRIB"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseTriB);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRA"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseSqA);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRB"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseSqB);
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRC"]) {
        NSLog(@"table: %d; delegate: %d", self.trainingTable.timeBase, self.appDelegate.timeBaseSqC);
    }
}


#pragma mark - time-base slider functions
- (IBAction)valueChanged:(id)sender {
    [self.sldrTimeBase setValue:[self roundSliderValue:self.sldrTimeBase.value] animated:NO];
    [self setTimeBaseLabel];
    [self.trainingTable reinit:(int)self.sldrTimeBase.value];
    [self.tableView reloadData];
    [self setAppDelegateVariables];
}

- (IBAction)sldrRepeatsChanged:(id)sender {
    [self.sldrRepeats setValue:[self roundSliderValue:self.sldrRepeats.value] animated:NO];
    [self setRepeatsLabel];
    TrainingTableEx *tt = (TrainingTableEx*)self.trainingTable;
    [tt reinitWithRepeats:self.sldrRepeats.value];
    [self.tableView reloadData];
    [self setAppDelegateVariables];
}

-(int)roundSliderValue:(float)x {
    if (x - (int)x > 0.5) return ((int)x + 1);
    else return (int) x;
}

- (void) setTimeBaseLabel {
    NSString *s1 = NSLocalizedString(@"Time Base", nil);
    NSString *s2 = NSLocalizedString(@"sec"      , nil);
    self.lblTimeBase.text = [NSString stringWithFormat:@"%@: %d %@", s1, (int)self.sldrTimeBase.value, s2];
}

- (void) setRepeatsLabel {
    NSString *s = NSLocalizedString(@"Repeats"      , nil);
    self.lblRepeats.text = [NSString stringWithFormat:@"%@: %d", s, (int)self.sldrRepeats.value];
}

-(void) setAppDelegateVariables {
//    NSLog(@"TABLE NAME: %@", [self.trainingTable.tableName uppercaseString]);
    if ([[self.trainingTable.tableName uppercaseString]        isEqualToString:@"CO2"]) {
        self.appDelegate.timeBaseCO2 = self.trainingTable.timeBase;
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"O2"]) {
        self.appDelegate.timeBaseO2 = self.trainingTable.timeBase;
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"TRIA"]) {
        self.appDelegate.timeBaseTriA = self.trainingTable.timeBase;
        self.appDelegate.repeatsTriA  = [((TrainingTableEx*)self.trainingTable).cycles[0] intValue];
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"TRIB"]) {
        self.appDelegate.timeBaseTriB = self.trainingTable.timeBase;
        self.appDelegate.repeatsTriB  = [((TrainingTableEx*)self.trainingTable).cycles[0] intValue];
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRA"]) {
        self.appDelegate.timeBaseSqA  = self.trainingTable.timeBase;
        self.appDelegate.repeatsSqA   = [((TrainingTableEx*)self.trainingTable).cycles[0] intValue];
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRB"]) {
        self.appDelegate.timeBaseSqB  = self.trainingTable.timeBase;
        self.appDelegate.repeatsSqB   = [((TrainingTableEx*)self.trainingTable).cycles[0] intValue];
    } else if ([[self.trainingTable.tableName uppercaseString] isEqualToString:@"SQRC"]) {
        self.appDelegate.timeBaseSqC  = self.trainingTable.timeBase;
        self.appDelegate.repeatsSqC   = [((TrainingTableEx*)self.trainingTable).cycles[0] intValue];
    }
    self.appDelegate.needsSave = YES;
}

#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.trainingTable.steps.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.trainingTable.phases.count == 2) {
        if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
            return [self getCellEx2:tableView idx:indexPath];
        } else {
            return [self getCell2:tableView idx:indexPath];
        }
    } else if (self.trainingTable.phases.count == 3) {
        if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
            return [self getCellEx3:tableView idx:indexPath];
        } else {
            return [self getCell3:tableView idx:indexPath];
        }
    } else if (self.trainingTable.phases.count == 4) {
        if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
            return [self getCellEx4:tableView idx:indexPath];
        } else {
            return [self getCell4:tableView idx:indexPath];
        }
    } else {
        if ([self.trainingTable isKindOfClass:[TrainingTableEx class]]) {
            return [self getCellExSimple:tableView idx:indexPath];
        } else {
            return [self getCellSimple:tableView idx:indexPath];
        }
    }
}

#pragma mark - Different Cell Types Generators

-(UITableViewCell*) getCellSimple:(UITableView *)tv idx:(NSIndexPath *)idx {
    
    TrainingTableCellSimple *cell = [tv dequeueReusableCellWithIdentifier:@"CellSimple" forIndexPath:idx];
    
    cell.lblDescription.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDuration.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblCycles.hidden    = YES;
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UITableViewCell*) getCellExSimple:(UITableView *)tv idx:(NSIndexPath *)idx {
    TrainingTableCellSimple *cell = [tv dequeueReusableCellWithIdentifier:@"CellSimple" forIndexPath:idx];
    
    cell.lblDescription.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDuration.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];

    TrainingTableEx *tex     = (TrainingTableEx *)self.trainingTable;
    cell.lblCycles.text      = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    cell.lblCycles.hidden    = NO;
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UITableViewCell*) getCell2:(UITableView *)tv idx:(NSIndexPath *)idx {

    TrainingTableCell2 *cell = [tv dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UITableViewCell*) getCellEx2:(UITableView *)tv idx:(NSIndexPath *)idx {
    TrainingTableCellEx2 *cell = [tv dequeueReusableCellWithIdentifier:@"CellEx2" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    
    TrainingTableEx *tex = (TrainingTableEx *)self.trainingTable;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UITableViewCell*) getCell3:(UITableView *)tv idx:(NSIndexPath *)idx {
    
    TrainingTableCell3 *cell = [tv dequeueReusableCellWithIdentifier:@"Cell3" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    cell.lblDescription3.text = [self.trainingTable getPhaseAbbreviation:2];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:2 forStep:(int)idx.row]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UITableViewCell*) getCellEx3:(UITableView *)tv idx:(NSIndexPath *)idx {
    TrainingTableCellEx3 *cell = [tv dequeueReusableCellWithIdentifier:@"CellEx3" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    cell.lblDescription3.text = [self.trainingTable getPhaseAbbreviation:2];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:2 forStep:(int)idx.row]];
    
    TrainingTableEx *tex = (TrainingTableEx *)self.trainingTable;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(UITableViewCell*) getCell4:(UITableView *)tv idx:(NSIndexPath *)idx {
    
    TrainingTableCell4 *cell = [tv dequeueReusableCellWithIdentifier:@"Cell4" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    cell.lblDescription3.text = [self.trainingTable getPhaseAbbreviation:2];
    cell.lblDescription4.text = [self.trainingTable getPhaseAbbreviation:3];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:2 forStep:(int)idx.row]];
    cell.lblDuration4.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:3 forStep:(int)idx.row]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

-(UITableViewCell*) getCellEx4:(UITableView *)tv idx:(NSIndexPath *)idx {
    TrainingTableCellEx4 *cell = [tv dequeueReusableCellWithIdentifier:@"CellEx4" forIndexPath:idx];
    
    cell.lblDescription1.text = [self.trainingTable getPhaseAbbreviation:0];
    cell.lblDescription2.text = [self.trainingTable getPhaseAbbreviation:1];
    cell.lblDescription3.text = [self.trainingTable getPhaseAbbreviation:2];
    cell.lblDescription4.text = [self.trainingTable getPhaseAbbreviation:3];
    
    cell.lblDuration1.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:0 forStep:(int)idx.row]];
    cell.lblDuration2.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:1 forStep:(int)idx.row]];
    cell.lblDuration3.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:2 forStep:(int)idx.row]];
    cell.lblDuration4.text    = [Tools secondsToTimeStr:[self.trainingTable getPhaseDuration:3 forStep:(int)idx.row]];
    
    TrainingTableEx *tex = (TrainingTableEx *)self.trainingTable;
    cell.lblCycles.text       = [NSString stringWithFormat:@"x%d", [tex.cyclesInitial[idx.row] intValue]];
    
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

@end
