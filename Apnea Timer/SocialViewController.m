//
//  SocialViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 08.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import <Social/Social.h>

#import "SocialViewController.h"
#import "CSPausibleTimer.h"
#import "SocialSharing.h"

#import "Flurry.h"

#define APPLINK @"http://apneatimer.slink.ws"

@interface SocialViewController()

@property (strong, nonatomic) UIView *overlayView;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation SocialViewController

#pragma mark - Social Sharing

//#define __USE_ACTIVITY_VIEW__
- (IBAction) actionShareButtonPressed:(id)sender {
    if ([self.appDelegate.timer isActive]) return;
#ifdef __USE_ACTIVITY_VIEW__
    NSArray *activityItems;
    NSString *text = [self getShareText];
    UIImage *image = [self getShareImage];
    if (image != nil) {
        activityItems = @[text, image];
    } else {
        activityItems = @[text];
    }
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
    [self presentViewController:activityController
                       animated:YES completion:nil];
#else
    UIActionSheet *sheet = [[UIActionSheet alloc]
                            initWithTitle:nil//@"share"
                            delegate:self
                            cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                            destructiveButtonTitle:nil
                            otherButtonTitles:
                            NSLocalizedString(@"Share on Facebook", nil),
                            NSLocalizedString(@"Share on Twitter", nil),
                            nil];
    
    sheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    sheet.tag = 1;
    
    //    UIView *barButtonView = [self.shareButton valueForKey:@"view"];
    //    CGRect buttonRect = barButtonView.frame;
    //    [sheet showFromRect:buttonRect inView:self.view animated:YES];
    
    UITabBarController *MyTabController = (UITabBarController *)((AppDelegate*) [[UIApplication sharedApplication] delegate]).window.rootViewController;
    [sheet showFromTabBar:MyTabController.tabBar];
    
#endif
}

#ifndef __USE_ACTIVITY_VIEW__
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            // share facebook
            [self shareFacebook:NSLocalizedString(@"share_text", nil)];
        }
            break;
        case 1: {
            // share twitter
            [self shareTwitter:NSLocalizedString(@"share_text", nil)];
        }
            break;
        default:
            break;
    }
}
#endif

-(void) shareFacebook:(NSString*) message {
    [self showIndicator];
    [SocialSharing shareOnFacebook:self andLink:APPLINK andMessage:message];
    //    NSLog(@"SHARE FB: %@", message);
}

-(void) shareTwitter:(NSString*) message {
    [self showIndicator];
    [SocialSharing shareOnTwitter:self andLink:APPLINK andMessage:message];
    //    NSLog(@"SHARE TWT: %@", message);
}

-(void) messageBox:(NSString*)message withTitle:(NSString*)title needShow:(BOOL) needShow {
    if (needShow) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:title
                                                             message:message
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [errorAlert show];
    }
    [self hideIndicator];
}

-(void) hideIndicator {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    [self.overlayView removeFromSuperview];
}

-(void) showIndicator {
    self.overlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.overlayView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.center = self.overlayView.center;
    [self.overlayView addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];
    [self.tabBarController.view addSubview:self.overlayView];
}

@end
