//
//  AppDelegate.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "AppDelegate.h"
#import "CSPausibleTimer.h"
#import "ShrinkViewProtocol.h"

#import "CO2TableClass.h"
#import "TrainingTable.h"

#import "Flurry.h"
#import "NSTimer+Block.h"

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate()
{
}
@end

@implementation AppDelegate

#pragma mark - iAd support
- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
//    NSLog(@"banner did load");
    if (!self.bannerIsVisible) {
        self.bannerIsVisible = YES;
        [[self getCurrentViewController] shrinkViewAnimated:YES];
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
//    NSLog(@"fail to receive ad");
}

- (BOOL) bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    if ([self.timer isActive]) {
        [self.timer pause];
    }
    return YES;
}

- (void) bannerViewActionDidFinish:(ADBannerView *)banner {
    if (self.timer.isPaused) {
        [self.timer start];
    }
}

#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    
#ifdef TEST_BUILD
    [Flurry startSession:@"TY2KFD779WXSHB42S7R9"]; // development
#else
    [Flurry startSession:@"7JNM2BRK4WMY557J323R"]; // distribution
#endif

    //    [Flurry setLogLevel:FlurryLogLevelAll];
    
    self.tabBarVC = (UITabBarController *)self.window.rootViewController;
    self.tabBarVC.delegate = self;
    
    [self loadSettings];
    //self.purchased       = false; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    if (!self.purchased) {
        self.adBanner = [[ADBannerView alloc] init];
        self.adBanner.delegate = self;
    }
    
    // try to save settings if needed once in 5 sec
    [NSTimer scheduledTimerWithTimeInterval:5
                                    repeats:YES
                                      block:(
                                             ^{
                                                 if(self.needsSave) {
                                                     [self saveSettings];
                                                     [self setNeedsSave:NO];
                                                 }
                                             })];
    
    return YES;
}

- (void) hideAdBanner {
    [[self getCurrentViewController] expandViewAnimated:YES];
}

//- (void) testTrainingTable {
//    TrainingTable *tt = [[CO2TableClass alloc] init];
//    [tt reset]; [tt nextPhase];
//    [self logData:0 table:tt];
//    for (int i = 1; i < 8; i++) {
//        [tt nextPhase];
//        [self logData:i table:tt];
//    }
//}

-(void) logData:(int)idx table:(TrainingTable*)tt{
    NSLog(@"%d. (step %d) %@ (%d) -> %@",
          idx,
          tt.currentStep,
          [tt getPhaseDescription:tt.currentPhase],
          tt.currentPhase,
          [tt getNextPhaseDescription]);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    [self ga_sessionEnd];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate.
    // Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - TabBar Delegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {

    NSArray *tabViewControllers = tabBarController.viewControllers;
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = viewController.view;
    if (fromView == toView)
        return NO;

    NSUInteger toIndex = [tabViewControllers indexOfObject:viewController];

    [UIView transitionFromView:fromView
                        toView:toView
                      duration:0.3
                       options: UIViewAnimationOptionTransitionCrossDissolve
                    completion:^(BOOL finished) {
                        if (finished) {
                            tabBarController.selectedIndex = toIndex;
                        }
                    }];
    return YES;
}

#pragma mark - Utilities
- (void) disableTabBar {
    UITabBarItem *current = [[self.tabBarVC tabBar] selectedItem];
    for (UITabBarItem *item in [[self.tabBarVC tabBar] items]) {
        if (item != current)
            [item setEnabled:NO];
    }
}

- (void) enableTabBar {
    for (UITabBarItem *item in [[self.tabBarVC tabBar] items]) {
        [item setEnabled:YES];
    }
}

-(UIViewController<ShrinkViewProtocol>*) getCurrentViewController {
    UIViewController *rootViewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ([rootViewController isKindOfClass:[UITabBarController class]])
        rootViewController = ((UITabBarController *)rootViewController).selectedViewController;
    if ([rootViewController isKindOfClass:[UINavigationController class]])
        rootViewController = ((UINavigationController *)rootViewController).topViewController;
    return (UIViewController<ShrinkViewProtocol>*)rootViewController;
}

#pragma mark - User Setting

- (void)saveSettings {
//    NSLog(@"saving...");
    [[NSUserDefaults standardUserDefaults] setInteger:(self.purchased) ? 1 : 0
                                               forKey: @"adsPurchased"];
    [[NSUserDefaults standardUserDefaults] setInteger:(self.useSound) ? 1 : 0
                                               forKey: @"useSound"];

    [[NSUserDefaults standardUserDefaults] setInteger:(self.useAdditionalSound) ? 1 : 0
                                               forKey: @"useAdditionalSound"];

    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseO2
                                               forKey: @"timeBaseO2"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseCO2
                                               forKey: @"timeBaseCO2"];

    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseTriA
                                               forKey: @"timeBaseTriA"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsTriA
                                               forKey: @"repeatsTriA"];
    
    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseTriB
                                               forKey: @"timeBaseTriB"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsTriB
                                               forKey: @"repeatsTriB"];

    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqA
                                               forKey: @"timeBaseSqA"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqA
                                               forKey: @"repeatsSqA"];

    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqB
                                               forKey: @"timeBaseSqB"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqB
                                               forKey: @"repeatsSqB"];

    [[NSUserDefaults standardUserDefaults] setInteger:self.timeBaseSqC
                                               forKey: @"timeBaseSqC"];
    [[NSUserDefaults standardUserDefaults] setInteger:self.repeatsSqC
                                               forKey: @"repeatsSqC"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) loadSettings {
    bool doSave = false;
    int val;
    if ([self loadIntParam:@"useSound"     into:&val notFound: 1]) doSave= true; self.useSound     = (val == 1) ? YES : NO;
    if ([self loadIntParam:@"useAdditionalSound" into:&val notFound: 0]) doSave= true; self.useAdditionalSound = (val == 1) ? YES : NO;
    if ([self loadIntParam:@"adsPurchased" into:&val notFound: 0]) doSave= true; self.purchased    = (val == 1) ? YES : NO;
    if ([self loadIntParam:@"timeBaseO2"   into:&val notFound:10]) doSave= true; self.timeBaseO2   = val;
    if ([self loadIntParam:@"timeBaseCO2"  into:&val notFound:10]) doSave= true; self.timeBaseCO2  = val;
    if ([self loadIntParam:@"timeBaseTriA" into:&val notFound: 4]) doSave= true; self.timeBaseTriA = val;
    if ([self loadIntParam:@"repeatsTriA"  into:&val notFound:10]) doSave= true; self.repeatsTriA  = val;
    if ([self loadIntParam:@"timeBaseTriB" into:&val notFound: 4]) doSave= true; self.timeBaseTriB = val;
    if ([self loadIntParam:@"repeatsTriB"  into:&val notFound: 6]) doSave= true; self.repeatsTriB  = val;
    if ([self loadIntParam:@"timeBaseSqA"  into:&val notFound: 4]) doSave= true; self.timeBaseSqA  = val;
    if ([self loadIntParam:@"repeatsSqA"   into:&val notFound: 8]) doSave= true; self.repeatsSqA   = val;
    if ([self loadIntParam:@"timeBaseSqB"  into:&val notFound: 4]) doSave= true; self.timeBaseSqB  = val;
    if ([self loadIntParam:@"repeatsSqB"   into:&val notFound: 6]) doSave= true; self.repeatsSqB   = val;
    if ([self loadIntParam:@"timeBaseSqC"  into:&val notFound: 5]) doSave= true; self.timeBaseSqC  = val;
    if ([self loadIntParam:@"repeatsSqC"   into:&val notFound: 6]) doSave= true; self.repeatsSqC   = val;
    if (doSave)
        [self saveSettings];
}

-(BOOL) loadIntParam:(NSString*)name_ into:(int*)val_ notFound:(int)default_ {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:name_]) {
        *val_ = default_;
        return true;
    } else {
        *val_ = (int)[[NSUserDefaults standardUserDefaults] integerForKey:name_];
        return false;
        NSLog(@"%@: %i", name_, *val_);
    }
}

@end
