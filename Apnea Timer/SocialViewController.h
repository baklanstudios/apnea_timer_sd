//
//  SocialViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 08.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "ViewControllerPrototype.h"
#import "SocialSharing.h"

@interface SocialViewController : ViewControllerPrototype <UIActionSheetDelegate, MessageBoxDelegate>

- (IBAction) actionShareButtonPressed:(id)sender;

@end
