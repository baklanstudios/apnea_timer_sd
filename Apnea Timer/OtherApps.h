//
//  OtherApps.h
//  Apnea Timer
//
//  Created by Mikhail on 08.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "SocialViewController.h"

@interface OtherApps : SocialViewController <UITableViewDelegate, UITableViewDataSource>

@end
