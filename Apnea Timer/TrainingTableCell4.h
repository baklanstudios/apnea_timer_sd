//
//  TrainingTableCell4.h
//  Apnea Timer
//
//  Created by Mikhail on 13.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableCell3.h"

@interface TrainingTableCell4 : TrainingTableCell3

@property (weak, nonatomic) IBOutlet UILabel *lblDuration4;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription4;

@end
