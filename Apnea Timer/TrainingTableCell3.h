//
//  TrainingTableCell3.h
//  Apnea Timer
//
//  Created by Mikhail on 13.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableCell2.h"

@interface TrainingTableCell3 : TrainingTableCell2

@property (weak, nonatomic) IBOutlet UILabel *lblDuration3;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription3;

@end
