//
//  SettingsViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 21.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "Flurry.h"

#define kRemoveAdsProductIdentifier @"ws.slink.apneatimer.removeads"

@interface SettingsViewController ()
{
    CGRect initialRect;
}

@property (weak, nonatomic) IBOutlet UIView *viewSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveAds;
@property (weak, nonatomic) IBOutlet UIButton *btnRestorePurchase;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actPurchase;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actRestore;
@property (weak, nonatomic) IBOutlet UISwitch *swtSound;
@property (weak, nonatomic) IBOutlet UISwitch *swtAddSound;

@end

@implementation SettingsViewController

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#pragma mark - View Controller

- (void)viewDidLoad {
    [super viewDidLoad];

//    [self initActivityIndicator];
//    [self initResultMessage];
//    self.screenName = @"Settings Screen";
    
    self.actPurchase.hidden = YES;
    self.actRestore.hidden = YES;
    
    [[self.btnRemoveAds layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [[self.btnRestorePurchase layer] setBorderColor:[[UIColor lightGrayColor] CGColor]];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        [[self.btnRemoveAds layer] setMasksToBounds:YES];
        [[self.btnRemoveAds layer] setBorderWidth:1.0f];
        [[self.btnRemoveAds layer] setCornerRadius:5.0f];
        [[self.btnRestorePurchase layer] setMasksToBounds:YES];
        [[self.btnRestorePurchase layer] setBorderWidth:1.0f];
        [[self.btnRestorePurchase layer] setCornerRadius:5.0f];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.soundSwitch.on = self.appDelegate.useSound;

    [self setAdditionalSoundSwitch:self.appDelegate.useSound];

    if (self.appDelegate.purchased) {
        self.btnRemoveAds.hidden        = TRUE;
        self.btnRemoveAds.enabled       = NO;
        self.btnRestorePurchase.hidden  = TRUE;
        self.btnRestorePurchase.enabled = NO;
    }
}

#pragma mark - User Actions

- (void) setAdditionalSoundSwitch:(BOOL) enabled {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.9];
//    [UIView setAnimationDelay:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    [self.swtAddSound setEnabled:enabled];
    if (enabled) {
        self.swtAddSound.on = self.appDelegate.useAdditionalSound;
    } else {
        self.swtAddSound.on = NO;
    }

    [UIView commitAnimations];
}

- (IBAction)soundSettingChanged:(UISwitch *)sender {
    if (sender.on) {
        self.appDelegate.useSound = true;
        [self setAdditionalSoundSwitch:YES];
    } else {
        self.appDelegate.useSound = false;
        [self setAdditionalSoundSwitch:NO];
    }
    self.appDelegate.needsSave = YES;
}

- (IBAction)additionalSoundSettingChange:(UISwitch *)sender {
    if (sender.on) {
        self.appDelegate.useAdditionalSound = YES;
    } else {
        self.appDelegate.useAdditionalSound = NO;
    }
    self.appDelegate.needsSave = YES;
}

- (IBAction)removeAdsBtnPress {
    [self disableControls:self.btnRemoveAds];
//    return;
    
    if([SKPaymentQueue canMakePayments]){
        //NSLog(@"User can make payments");
        SKProductsRequest *productsRequest =
        [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:kRemoveAdsProductIdentifier]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
    else{
//        NSLog(@"User cannot make payments due to parental controls");
        //this is called the user cannot make payments, most likely due to parental controls
    }
}

- (IBAction) restorePurchase{
    [self disableControls:self.btnRestorePurchase];
//    return;
    
    //this is called when the user restores purchases, you should hook this up to a button
    //NSLog(@"Trying to restore a purchase");
    //    [self.activity show];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark - Utilities

-(void) removeAds {
    self.appDelegate.purchased = TRUE;
    self.appDelegate.bannerIsVisible = FALSE;
    self.btnRemoveAds.hidden = YES;
    self.btnRemoveAds.enabled = NO;
    self.btnRestorePurchase.hidden = YES;
    self.btnRestorePurchase.enabled = NO;
    [self.appDelegate hideAdBanner];
    [self.appDelegate saveSettings];
}

-(void) disableControls:(id)sender {
    [self.appDelegate disableTabBar];
    self.swtSound.enabled = NO;
    self.btnRemoveAds.enabled = NO;
    self.btnRestorePurchase.enabled = NO;
    if (sender == self.btnRemoveAds) {
        self.actRestore.hidden = YES;
        [self.actRestore stopAnimating];
        self.actPurchase.hidden = NO;
        [self.actPurchase startAnimating];
    } else {
        self.actRestore.hidden = NO;
        [self.actRestore startAnimating];
        self.actPurchase.hidden = YES;
        [self.actPurchase stopAnimating];
    }
    
    [NSTimer scheduledTimerWithTimeInterval:5
                                     target:self
                                   selector:@selector(enableControls)
                                   userInfo:nil
                                    repeats:NO];
}

-(void) enableControls {
    [self.appDelegate enableTabBar];
    self.swtSound.enabled = YES;
    self.btnRemoveAds.enabled = YES;
    self.btnRestorePurchase.enabled = YES;
    self.actRestore.hidden = YES;
   [self.actRestore stopAnimating];
    self.actPurchase.hidden = YES;
   [self.actPurchase stopAnimating];
}

-(void) purchaseError {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"Error", nil)
                          message:NSLocalizedString(@"Purchase Error", nil)
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil, nil];
    [alert show];
    [self enableControls];
}

-(void) restoreError {
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:NSLocalizedString(@"Error", nil)
                               message:NSLocalizedString(@"Restore Error", nil)
                               delegate:nil
                               cancelButtonTitle:NSLocalizedString(@"OK", nil)
                               otherButtonTitles:nil];
    [errorAlert show];
    [self enableControls];
}

# pragma mark - In-App purchase

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if(count > 0) {
        validProduct = [response.products objectAtIndex:0];
        //        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct) {
        [self enableControls];
        //        NSLog(@"No products available");
    }
}

- (IBAction)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for(SKPaymentTransaction *transaction in transactions){
        if (transaction.transactionState == SKPaymentTransactionStatePurchasing) {
                        NSLog(@"Transaction state -> Purchasing");
        } else if (transaction.transactionState == SKPaymentTransactionStatePurchased) {
             NSLog(@"Transaction state -> Purchased (A)");
            [self removeAds];
            [Flurry logEvent:@"purchased_ads_removal"];
            [self enableControls];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
        } else if (transaction.transactionState == SKPaymentTransactionStateRestored) {
            NSLog(@"Transaction state -> Restored on Purchase");
            //            [self removeAds];
            //            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            //            [self setControlsEnabled:YES];
        } else if (transaction.transactionState == SKPaymentTransactionStateFailed) {
            //called when the transaction does not finnish
                        NSLog(@"Transaction state -> Failed");
            if(transaction.error.code != SKErrorPaymentCancelled){
                                NSLog(@"Transaction state -> Cancelled");
            }
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            [self enableControls];
        }
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    //    NSLog(@"Error purchasing: %@", error);
    [self purchaseError];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    //    NSLog(@"received restored transactions: %i", (int)queue.transactions.count);
    for (SKPaymentTransaction *transaction in queue.transactions) {
        if(SKPaymentTransactionStateRestored){
            //            NSLog(@"Transaction state -> Restored");
            [self removeAds];
            [Flurry logEvent:@"restored_ads_removal"];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            [self enableControls];
            break;
        }
    }
    if ((int)queue.transactions.count == 0)
        [self restoreError];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    //    NSLog(@"Error restoring purchases: %@", error);
    [self restoreError];
}

@end
