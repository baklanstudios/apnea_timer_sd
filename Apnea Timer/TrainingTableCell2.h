//
//  TrainingTableCell2TableViewCell.h
//  Apnea Timer
//
//  Created by Mikhail on 13.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingTableCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDuration1;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription1;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration2;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription2;

@end
