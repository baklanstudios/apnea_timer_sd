//
//  TrainingTableViewCellA.h
//  Apnea Timer
//
//  Created by Mikhail on 09.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrainingTableCellSimple : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDuration;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblCycles;

@end
