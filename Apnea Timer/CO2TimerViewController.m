//
//  ViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//


#import "CO2TimerViewController.h"
#import "CO2TableClass.h"
#import "Tools.h"
#import "AppDelegate.h"

@interface CO2TimerViewController ()
@end

@implementation CO2TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSLog(@"TIME BASE: %d", self.appDelegate.timeBaseCO2);
//    self.trainingTable = [[CO2TableClass alloc] init];
    self.trainingTable = [[CO2TableClass alloc] initWithTimeBase:self.appDelegate.timeBaseCO2];
    [super viewDidLoad];
}

- (IBAction)btnStartPressed:(id)sender {
    [self actionFirstButtonPressed];
}

- (IBAction)btnResetPressed:(id)sender {
    [self actionSecondButtonPressed];
}

@end
