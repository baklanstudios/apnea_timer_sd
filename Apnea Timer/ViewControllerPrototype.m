//
//  ViewControllerPrototype.m
//  Apnea Timer
//
//  Created by Mikhail on 02.02.15.
//  Copyright (c) 2015 ecg. All rights reserved.
//

#import "ViewControllerPrototype.h"

@implementation ViewControllerPrototype

#pragma mark - View Controller

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

-(void) viewDidLoad {
    [super viewDidLoad];
    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.container.backgroundColor = [UIColor whiteColor];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        self.navigationItem.leftBarButtonItem.image = [UIImage imageNamed:@"sharew"];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.container addSubview:self.appDelegate.adBanner];
    if (self.appDelegate.bannerIsVisible && self.cnstrContainerHeight.constant == 0) {
        [self shrinkViewAnimated:NO];
    } else if(!self.appDelegate.bannerIsVisible) {
        [self expandViewAnimated:NO];
    }
}

-(void) addSubViewConstraint:(NSString*)str
                      toView:target
                    withDict:dict {
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:str options:0 metrics:nil views:dict];
    [target addConstraints:constraints];
}

#pragma mark - Shrink View Protocol

- (void) shrinkViewAnimated:(BOOL)animated {
    if (!self.appDelegate.purchased) {
        self.cnstrContainerHeight.constant = 50;
        if (animated)
            [UIView animateWithDuration:.5
                             animations:^{
                                 [self.view layoutIfNeeded];
                             }];
    }
}

- (void) expandViewAnimated:(BOOL)animated {
    self.cnstrContainerHeight.constant = 0;
    if (animated)
        [UIView animateWithDuration:.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
}

@end
