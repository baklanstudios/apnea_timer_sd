//
//  TTTimerView.h
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UITimerViewController.h"
#import "TrainingTable.h"

@class AppDelegate;

@interface TTTimerViewController : UITimerViewController

@end

@interface TTTimerViewController (Protected)

-(void) actionFirstButtonPressed;
-(void) actionSecondButtonPressed;

@end