//
//  ShrinkViewProtocol.h
//  Apnea Timer
//
//  Created by Mikhail on 15.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#ifndef Apnea_Timer_ShrinkViewProtocol_h
#define Apnea_Timer_ShrinkViewProtocol_h

@protocol ShrinkViewProtocol
- (void) shrinkViewAnimated:(BOOL)animated;
- (void) expandViewAnimated:(BOOL)animated;
@end

#endif
