//
//  TrainingTablesViewController.h
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialViewController.h"

@class AppDelegate;

@interface TTListViewController : SocialViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *trainingTables;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
