//
//  O2TimerViewController.m
//  Apnea Timer
//
//  Created by Mikhail on 06.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "O2TimerViewController.h"
#import "O2TableClass.h"
#import "Tools.h"
#import "AppDelegate.h"

@interface O2TimerViewController()

@end

@implementation O2TimerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    self.trainingTable = [[O2TableClass alloc] init];
    self.trainingTable = [[O2TableClass alloc] initWithTimeBase:self.appDelegate.timeBaseO2];
    [super viewDidLoad];
}

- (IBAction)btnStartPressed:(id)sender {
    [self actionFirstButtonPressed];
}

- (IBAction)btnResetPressed:(id)sender {
    [self actionSecondButtonPressed];
}

@end
