//
//  TrainingTableCellEx3.h
//  Apnea Timer
//
//  Created by Mikhail on 13.09.14.
//  Copyright (c) 2014 ecg. All rights reserved.
//

#import "TrainingTableCell3.h"

@interface TrainingTableCellEx3 : TrainingTableCell3

@property (weak, nonatomic) IBOutlet UILabel *lblCycles;

@end
